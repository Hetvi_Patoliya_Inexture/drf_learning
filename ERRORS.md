### Errors Faced (Keep in Mind)

```commandline
APPEND_SLASH

Default: True

When set to True, if the request URL does not match any of the patterns in
 the URLconf and it doesn't end in a slash, 
 an HTTP redirect is issued to the same URL with a slash appended. 
 Note that the redirect may cause any data submitted in a POST request to be lost.
 
 How to recreate:
 Hit Api with method which is not allowed(eg: delete) and not put/at the end of url
```
