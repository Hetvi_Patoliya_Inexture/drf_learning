from rest_framework.renderers import JSONRenderer


class CustomResponseRenderer(JSONRenderer):
    """
    Custom Renderer Class, Response format for API

    REST framework includes a number of built in Renderer classes,
    that allow you to return responses with various media types.
    There is also support for defining your own custom renderers,
    which gives you the flexibility to design your own media types.
    """

    def render(self, data, accepted_media_type=None, renderer_context=None):
        status_code = renderer_context['response'].status_code
        response_data = {
            "status": "success",
            "status_code": status_code,
            "message": data.pop("message", "Something Went Wrong!"),
        }
        if not str(status_code).startswith('2'):
            response_data["status"] = "failure"
            response_data["error"] = data
        else:
            response_data["data"] = data.get('data', None)
        return super(CustomResponseRenderer, self).render(response_data, accepted_media_type, renderer_context)
