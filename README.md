### For Custom Model

AUTH_USER_MODEL = 'user.CustomUser' (add in settings.py)


### JWT Authentication

- Follow the required steps from the doc
- https://django-rest-framework-simplejwt.readthedocs.io/en/latest/getting_started.html

### Custom Renderer class
- Here used for Same response Format for all the API responses
- Added Custom Renderer class to utils.py
- Added to settings.py to apply to All API response 
```commandline
REST_FRAMEWORK = {
   ...
    'DEFAULT_RENDERER_CLASSES': [
        "utils.CustomRenderer"
    ]
}
```




