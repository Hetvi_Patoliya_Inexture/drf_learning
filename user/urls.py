from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from user import views
from user.views import MyTokenView

urlpatterns = [
    path('fbv/', views.user_services, name='user-services'),
    path('fbv/<uuid:pk>/', views.user_service, name='user-service'),
    path('login/', MyTokenView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
