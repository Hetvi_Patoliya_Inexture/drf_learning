from rest_framework import serializers

from constatnts import PASSWORD_REGEX, ERR_INVALID_PASSWORD
from user.models import CustomUser
from validators import regex_validator


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['id', 'username', 'email', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def validate_password(self, value):
        return regex_validator(pattern_str=PASSWORD_REGEX, value=value, err_message=ERR_INVALID_PASSWORD)

    def create(self, validated_data):
        """
        why do we require to override password and is_active field?
        here is_active=true is default but in case if not that need to change
        but password should be hashed

        https://stackoverflow.com/questions/55906891/django-drf-simple-jwt-authenticationdetail-no-active-account-found-with-the
        """
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)

        instance.is_active = True
        if password is not None:
            # Set password does the hash, so we don't need to call make_password
            instance.set_password(password)
        instance.save()
        return instance
