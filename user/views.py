from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenObtainPairView

from constatnts import MSG_LIST_USER, MSG_REGISTER_USER_SUCCESSFULLY, \
    ERR_USER_NOT_EXISTS, MSG_RETRIEVE_USER, MSG_LOG_IN_SUCCESSFULLY, MSG_USER_UPDATED_SUCCESSFULLY
from user.models import CustomUser
from user.serializers import UserSerializer


# Create your views here.
@api_view(['GET', 'POST'])
def user_services(request):
    """
    It register the user if request method is POST
    And if request methods is GET then list all register users


    """

    # Function based view (without id)
    if request.method == 'GET':
        users = CustomUser.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(data={"message": MSG_LIST_USER, "data": serializer.data})
    elif request.method == 'POST':
        # register user
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(data={"message": MSG_REGISTER_USER_SUCCESSFULLY, "data": serializer.data},
                            status=status.HTTP_201_CREATED)


@api_view(['GET', 'PUT', 'PATCH', 'DELETE'])
@permission_classes((IsAuthenticated,))
def user_service(request, pk):
    """
    Function based view (with id)
    """
    try:
        user = CustomUser.objects.get(pk=pk)
    except CustomUser.DoesNotExist:
        return Response(data={"message": ERR_USER_NOT_EXISTS.format(pk)},
                        status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UserSerializer(user)
        return Response(data={"message": MSG_RETRIEVE_USER, "data": serializer.data})
    elif request.method == 'PUT':
        serializer = UserSerializer(user, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(data={"message": MSG_USER_UPDATED_SUCCESSFULLY, "data": serializer.data})
    elif request.method == 'PATCH':
        serializer = UserSerializer(user, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(data={"message": MSG_USER_UPDATED_SUCCESSFULLY, "data": serializer.data})
    elif request.method == 'DELETE':
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class MyTokenView(TokenObtainPairView):
    """
    User Login
    https://django-rest-framework-simplejwt.readthedocs.io/en/latest/getting_started.html
    We can directly use in url as shown in above doc,
    but to create custom response and to give message in response we have created the custom class
    """

    def post(self, request, *args, **kwargs):
        """ user login """
        response = super(MyTokenView, self).post(request, *args, **kwargs)
        return Response(data={"message": MSG_LOG_IN_SUCCESSFULLY, "data": response.data})










