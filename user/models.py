import uuid

from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.db import models

from constatnts import USER_NAME_REGEX, ERR_INVALID_USERNAME


# Create your models here.
class CustomUser(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    first_name = None
    last_name = None
    username = models.CharField('username', unique=True, max_length=30,
                                validators=[RegexValidator(regex=USER_NAME_REGEX, message=ERR_INVALID_USERNAME)])
    email = models.EmailField('email address', unique=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return f"<{self.id}"
