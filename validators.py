import re

from rest_framework.exceptions import ValidationError


def extra_fields_validator(obj, data):
    if hasattr(obj, 'initial_data'):
        if unknown_keys := set(obj.initial_data.keys()) - set(obj.fields.keys()):
            raise ValidationError(f"Got unknown fields: {unknown_keys}")
    return data


def regex_validator(pattern_str=None, value=None, err_message=None):
    pattern = re.compile(pattern_str)
    if re.fullmatch(pattern, value):
        return value
    raise ValidationError(err_message)
