from django.http import Http404
from rest_framework.response import Response
from rest_framework.views import APIView

from constatnts import MSG_LIST_TODO, MSG_NO_TODOS_AVAILABLE, MSG_CREATED_TODO
from todo.models import Todo
from todo.serializers import TodoSerializer, TodoNestedSerializer


class TodoViewCL(APIView):
    def get(self, request):
        print(f"request::{request}")
        todos = Todo.objects.all()
        serializer = TodoNestedSerializer(todos, many=True)
        if data := serializer.data:
            return Response(data={"message": MSG_LIST_TODO, "data": data})
        return Response(data={"message": MSG_NO_TODOS_AVAILABLE, "data": data})

    def post(self, request):
        """
        why do we need context={"request": self.request} (because we have used
        creator = serializers.HiddenField(default=serializers.CurrentUserDefault()) in serializer
        https://stackoverflow.com/questions/27632385/django-rest-framework-currentuserdefault-use
        """
        serializer = TodoNestedSerializer(data=request.data, context={"request": request})
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(data={"message": MSG_CREATED_TODO, "data": serializer.data})


class TodoViewRUD(APIView):
    def get_object(self, pk):
        try:
            return Todo.objects.get(pk=pk)
        except Todo.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        todo = self.get_object(pk)
        serializer = TodoSerializer(todo)
        return Response(data={"message": MSG_LIST_TODO, "data": serializer.data})

    def put(self, request, pk):
        todo = self.get_object(pk)
        serializer = TodoNestedSerializer(instance=todo, data=request.data, context={"request": request}, )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(data={"message": MSG_CREATED_TODO, "data": serializer.data})
