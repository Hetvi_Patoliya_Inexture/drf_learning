from django.urls import path

from todo.views import TodoViewCL, TodoViewRUD

urlpatterns = [
    path('cbv/', TodoViewCL.as_view(), name='todo'),
    path('cbv/<uuid:pk>/', TodoViewRUD.as_view(), name='todo-id'),
]
