from rest_framework import serializers

from constatnts import TITLE_REGEX, ERR_INVALID_TITLE
from task.models import Task
from task.serializer import TaskSerializer
from todo.models import Todo
from user.serializers import UserSerializer
from validators import extra_fields_validator, regex_validator


class TodoSerializer(serializers.ModelSerializer):
    creator = UserSerializer()

    class Meta:
        model = Todo
        fields = ["id", 'title', "creator"]
        extra_kwargs = {
            'id': {'read_only': True}
        }


class TodoNestedSerializer(serializers.ModelSerializer):
    creator = serializers.HiddenField(default=serializers.CurrentUserDefault())
    tasks = TaskSerializer(many=True)

    def validate_title(self, value):
        return regex_validator(pattern_str=TITLE_REGEX, value=value, err_message=ERR_INVALID_TITLE)

    def validate(self, data):
        for e in self.initial_data['tasks']:
            extra_fields_validator(obj=TaskSerializer(data=e), data=data)
        return extra_fields_validator(obj=self, data=data)

    def create(self, validated_data):
        print("create")
        tasks = validated_data.pop('tasks', None)
        todo, created = Todo.objects.get_or_create(**validated_data)
        for task in tasks:
            Task.objects.create(todo=todo, **task)
        return todo

    def update(self, instance, validated_data):
        tasks = validated_data.pop('tasks', None)
        Todo.objects.filter(id=instance.id).update(**validated_data)
        for task in tasks:
            task_id = task.pop('id', None)
            Task.objects.filter(id=task_id, todo_id=instance.id).update(**task)
        return Todo.objects.get(id=instance.id)

    class Meta:
        model = Todo
        fields = ["id", 'title', 'creator', 'tasks']


