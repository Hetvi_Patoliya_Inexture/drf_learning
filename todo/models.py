import uuid

from django.db import models

from user.models import CustomUser


class Todo(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    title = models.CharField(max_length=50, unique=True)
    creator = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='todos')

    def __str__(self):
        return f"<{self.id}>"



