from rest_framework import serializers

from constatnts import TITLE_REGEX, ERR_INVALID_TITLE
from task.models import Task
from validators import regex_validator


class TaskSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(required=True)

    def validate_title(self, value):
        return regex_validator(pattern_str=TITLE_REGEX, value=value, err_message=ERR_INVALID_TITLE)

    class Meta:
        model = Task
        fields = ["id", 'title', 'description']


class TaskNestedUpdateSerializer(serializers.ModelSerializer):

    def validate_title(self, value):
        return regex_validator(pattern_str=TITLE_REGEX, value=value, err_message=ERR_INVALID_TITLE)

    class Meta:
        model = Task
        fields = ["id", 'title', 'description']
