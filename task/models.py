import uuid

from django.db import models

from todo.models import Todo


# Create your models here.
class Task(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    title = models.CharField(max_length=20)
    description = models.TextField()
    todo = models.ForeignKey(Todo, related_name='tasks', on_delete=models.CASCADE)

    def __str__(self):
        return f"<{self.id}>"
