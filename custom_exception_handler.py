from rest_framework.views import exception_handler
def custom_exception(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if (response is not None) and (response.status_code == 404):
        response.data['custom_field'] = 'some_custom_value'
    return response


